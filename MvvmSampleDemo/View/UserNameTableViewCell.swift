//
//  UserNameTableViewCell.swift
//  MvvmSampleDemo
//
//  Created by Praveen Reddy on 20/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit


protocol userNameTableViewCellDelegate: class {
    
    func addUser(cell: UserNameTableViewCell)
}

class UserNameTableViewCell: CommonTableViewCell {
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var addUserBtn: UIButton!
    @IBOutlet weak var userProfileImg: ImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    
    static func nib() -> UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    
    static func cellReuseIdentifier() -> String {
        return String(describing: self)
    }
    
 
    
    override func configure(_ item: Any?) {
        
        guard let model = item as? CellDataModel else { return }
        
        userName.text = model.userData.name
        
//        userProfileImg.sd_setImage(with: URL(string: model.userData.image ?? "image"), placeholderImage: UIImage(named: "placeholder.png"))
       
//        userProfileImg.setImageFromUrl(urlString: model.userData.image ?? "image")
        
        
    }
    
    @IBAction func tappingOn_AddUserBtn(_ sender: Any) {
        
        (self.delegate as? userNameTableViewCellDelegate)?.addUser(cell: self)
    }
    
    
}
