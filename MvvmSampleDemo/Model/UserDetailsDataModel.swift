//
//  UserDetailsDataModel.swift
//  MvvmSampleDemo
//
//  Created by Praveen Reddy on 29/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

struct UserDetailsDataModel : Codable {
    let name : String?
    let lastname : String?
    let mobile : String?
    let gender : String?
    let birth : String?
    let biodata : String?
    let image : String?
    let id : Int?
    
    enum CodingKeys: String, CodingKey {
        
        case name = "name"
        case lastname = "lastname"
        case mobile = "mobile"
        case gender = "gender"
        case birth = "birth"
        case biodata = "biodata"
        case image = "image"
        case id = "id"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        lastname = try values.decodeIfPresent(String.self, forKey: .lastname)
        mobile = try values.decodeIfPresent(String.self, forKey: .mobile)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        birth = try values.decodeIfPresent(String.self, forKey: .birth)
        biodata = try values.decodeIfPresent(String.self, forKey: .biodata)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
    }
    
}
