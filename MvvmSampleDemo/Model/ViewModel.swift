//
//  ViewModel.swift
//  MvvmSampleDemo
//
//  Created by Praveen Reddy on 29/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation


//--------------------------------------------------
// MarK : response model
//--------------------------------------------------

struct Model : Codable {
    
    let statusCode : Int?
    let error : String?
    let success : String?
    let username_results : [UserDetailsDataModel]?
    
    enum CodingKeys: String, CodingKey {
        
        case statusCode = "statusCode"
        case error = "error"
        case success = "success"
        case username_results = "username_results"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        error = try values.decodeIfPresent(String.self, forKey: .error)
        success = try values.decodeIfPresent(String.self, forKey: .success)
        username_results = try values.decodeIfPresent([UserDetailsDataModel].self, forKey: .username_results)
    }
    
}


//--------------------------------------------------
// MarK : FirstVCViewModel
//--------------------------------------------------

class FirstVCViewModel {

    let sharedInstance = Connection()
    
    var items: [CellDataModel] = []

    var reloadHandler: DataHandler = { }

    typealias DataHandler = () -> Void

    
    init() { }

    
    
    var itemCount: Int {
        return self.items.count
    }
    
    
    func item(_ indexPath: IndexPath) -> CellDataModel {
        return self.items[indexPath.row]
    }
    
    
    
    //--------------------------------------------------
    // MarK : API Calling
    //--------------------------------------------------
    
    func fetchItems(completion: @escaping (_ error: Error?) -> Void) {
        
        
        let URL = APIList().getUrlString(url: .USERPROFILE)
        
        self.requestData(url: URL, success:
            {
                (serverResponse) in
                
                guard let status = serverResponse.error else{
                    return
                }
                
                if status == "Valid Request" {
                    
                    guard let getItem = serverResponse.username_results else {
                        return
                    }
                    
                    self.configureModels(list: getItem)
                
                    print(getItem)
                    completion(nil)
                    
                }
                
        },
                         
                         failure:
            {
                (Error) in
                
//                                self.showAlertError(messageStr: Error.localizedDescription)
        })
        
    }
    
    
    
    //--------------------------------------------------
    // MarK : configuring Models
    //--------------------------------------------------
    
    private func configureModels(list: [UserDetailsDataModel]) {
        self.items = list.map { CellDataModel(userData: $0) }
        self.reloadHandler()
    }
    
    
}


extension FirstVCViewModel {
    
    
    func requestData(url :String,success:@escaping (Model) -> Void, failure:@escaping (Error) -> Void)
    {
        
        sharedInstance.requestGET(url, params: nil, headers: nil, success:
            {
                (JSON) in
                
                let  result :Data? = JSON
                
                if result != nil
                {
                    do
                    {
                        let response = try JSONDecoder().decode(Model.self, from: result!)
                        success(response)
                        print("GetResponse",response)
                        
                    }
                    catch let error as NSError
                    {
                        failure(error)
                    }
                }
                else
                {
                    
                }
                
        },
                                  failure:
            {
                (Error) in
                failure(Error)
        })
    }
    
}
