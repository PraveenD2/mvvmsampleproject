//
//  ViewController.swift
//  MvvmSampleDemo
//
//  Created by Praveen Reddy on 20/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //--------------------------------------------------
    // MarK : Storyboard variables
    //--------------------------------------------------
    @IBOutlet weak var tableView: UITableView!
    
    //--------------------------------------------------
    // MarK : Custom variables
    //--------------------------------------------------
    private var viewModel = FirstVCViewModel()

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        setupViewModel()
    }
    
    
    //--------------------------------------------------
    // MarK : setupTableView
    //--------------------------------------------------
    
    private func setupTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
//        self.tableView.register(UserNameTableViewCell.nib(), forCellReuseIdentifier: UserNameTableViewCell.cellReuseIdentifier())
        self.tableView.register(UINib(nibName: "UserNameTableViewCell", bundle: nil),
                                forCellReuseIdentifier: "UserNameTableViewCell")
    }
    
    
    //--------------------------------------------------
    // MarK : setupViewModel
    //--------------------------------------------------
    
    private func setupViewModel() {

        self.viewModel.reloadHandler = {
            self.tableView.reloadData()
        }

        self.showLoader()
        self.viewModel.fetchItems { _ in
            self.hideLoader()
        }
    }

}


//--------------------------------------------------
// MarK : UITableViewDelegate
//--------------------------------------------------

extension ViewController: UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView,
                   estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75.0//UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
}



//--------------------------------------------------
// MarK : UITableViewDataSource
//--------------------------------------------------

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        
        print(viewModel.itemCount)
        return viewModel.itemCount//self.viewModel[0].username_results?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(
            withIdentifier: "UserNameTableViewCell"
            ) as! UserNameTableViewCell
        cell.delegate = self
        cell.item = self.viewModel.item(indexPath)
        
        return cell
        
    }

    
}


//--------------------------------------------------
// MarK : userNameTableViewCellDelegate configure for cell button Action
//--------------------------------------------------

extension ViewController: userNameTableViewCellDelegate {

    func addUser(cell: UserNameTableViewCell) {

        print(cell.item as Any)
        
    }
    
}


