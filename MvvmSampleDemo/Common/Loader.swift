//
//  Loader.swift
//  MvvmSampleDemo
//
//  Created by Praveen Reddy on 20/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import Alamofire

import MBProgressHUD


//---------------------------------------------------------
// MARK: - loader Creation Functionality
//---------------------------------------------------------


class Loader {
    
    static func showAdded(to view: UIView, animated: Bool){
        MBProgressHUD.showAdded(to: view, animated: animated)
    }
    
    static func hide(for view: UIView, animated: Bool){
        MBProgressHUD.hide(for: view, animated: animated)
    }
}

extension UIViewController {
    
    func showLoader(animated: Bool = false) {
        Loader.showAdded(to: self.view, animated: animated)
    }
    
    func hideLoader(animated: Bool = false) {
        Loader.hide(for: self.view, animated: animated)
    }
}

//---------------------------------------------------------
// MARK: - internet connecti0n Checking Functionality
//---------------------------------------------------------


class Connectivity
{
    class func isConnectedToInternet() ->Bool
    {
        return NetworkReachabilityManager()!.isReachable
    }
}
