//
//  CommonTableViewCell.swift
//  MvvmSampleDemo
//
//  Created by Praveen Reddy on 20/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class CommonTableViewCell: UITableViewCell {

    var item: Any? {
        didSet {
            self.configure(self.item)
        }
    }
    
    weak var delegate: NSObjectProtocol? = nil
    
    func configure(_ item: Any?) { }

}
