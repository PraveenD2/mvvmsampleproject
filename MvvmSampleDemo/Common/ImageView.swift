//
//  ImageView.swift
//  MvvmSampleDemo
//
//  Created by Praveen Reddy on 20/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Foundation
import SDWebImage

public class ImageView: UIImageView {
    
    let activity = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorView.Style.whiteLarge)

    public override func awakeFromNib() {
        super.awakeFromNib()
//        self.addSubview(self.activity)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        activity.frame = self.bounds;

//        self.activity.frame = self.bounds;
    }
    
    
    
    public func setImageFromUrl(urlString: String, placeHolder: UIImage? = nil) {
        guard let url = URL(string: urlString) else {
            return
        }
        
        let options: SDWebImageOptions = [
            .continueInBackground,
            .highPriority,
            .progressiveDownload,
            .refreshCached,
            .retryFailed
        ]
        self.image = placeHolder
        
        
        self.startLoader()
        
        self.sd_setImage(
            with: url,
            placeholderImage: placeHolder,
            options: options) { [weak self] (image, _, _, _) in
                guard let this = self else { return }
                this.stopLoader()
        }
    }
    
    private func startLoader() {
        activity.isHidden = false
        activity.startAnimating()
    }
    
    private func stopLoader() {
        activity.isHidden = true
        activity.stopAnimating()
    }
}
