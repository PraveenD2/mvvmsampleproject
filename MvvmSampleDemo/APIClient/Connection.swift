//
//  Connection.swift
//  MVCDemo
//
//  Created by Pyramidions on 19/09/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class Connection
{
    
    func requestPOST(_ url: String, params : Parameters?, headers : HTTPHeaders?, success:@escaping (Data) -> Void, failure:@escaping (Error) -> Void)
    {
        
        print("URL = ",url)
        print("Parameter = ",params!)
        
        
        if Connectivity.isConnectedToInternet()
        {
            if headers == nil
            {
                Alamofire.request(url, method: .post, parameters: params!, encoding: JSONEncoding.default, headers: nil).responseJSON
                {
                        (responseObject) -> Void in
                        
                        print("Response = ",responseObject)
                        
                        switch responseObject.result
                        {
                            case .success:
                                if let data = responseObject.data
                                {
                                    success(data)
                                }
                            case .failure(let error):
                                failure(error)
                        }
                 
                }
            }
            else
            {
                
                print("Headers = ",headers!)

                Alamofire.request(url, method: .post, parameters: params!, encoding: JSONEncoding.default, headers: headers!).responseJSON
                    {
                        (responseObject) -> Void in
                        
                        print("Response = ",responseObject)
                        
                        switch responseObject.result
                        {
                        case .success:
                            if let data = responseObject.data
                            {
                                success(data)
                            }
                        case .failure(let error):
                            failure(error)
                        }
                    
                }
            }
            
        }
        else
        {
//            let err = NSError(domain: "Check Internet Connection".localized(), code: nil, userInfo: nil)
//            let error = NSError(domain: "", code: 4, userInfo: "Check Internet Connection")
////
//            failure(error)
        }
    }
    
    
    func requestGET(_ url: String, params : Parameters?,headers : [String : String]?, success:@escaping (Data) -> Void, failure:@escaping (Error) -> Void)
    {
        
        do
        {
            Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON
            {
                (response) in
                switch response.result
                {
                    case .success:
                        if let data = response.data
                        {
                            success(data)
                        }
                    case .failure(let error):
                        failure(error)
                }
            }

 
            
        }
        catch let JSONError as NSError
        {
            failure(JSONError)
        }
        
    }
    
}
